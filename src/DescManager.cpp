#include "DescManager.h"

#include "FlipflopDesc.h"
#include "GateDesc.h"

#include <functional>

namespace ak47
{

void
DescManager::AddDesc(const std::string& name, const CircuitDesc* core)
{
  _coreMap[name] = core;
}

const CircuitDesc*
DescManager::GetDesc(const std::string& name) const
{
  if(_coreMap.find(name)==_coreMap.end()) {
    return NULL;
  }
  return _coreMap.find(name)->second;
}

const DescManager*
GetGateDescManager()
{
  static DescManager* gateDescManager = NULL;
  if(gateDescManager==NULL) {
    gateDescManager = new DescManager();
    // Nullary
    gateDescManager->AddDesc("GATE:TRUE",
        new GateDesc0<true>("true"));
    gateDescManager->AddDesc("GATE:FALSE",
        new GateDesc0<false>("false"));
    // Unary
    gateDescManager->AddDesc("GATE:NOT",
        new GateDesc1<std::logical_not>("not gate"));
    // Binary
    gateDescManager->AddDesc("GATE:AND", 
        new GateDesc2<std::logical_and>("and gate"));
    gateDescManager->AddDesc("GATE:OR",
        new GateDesc2<std::logical_or>("or gate"));
    gateDescManager->AddDesc("GATE:NAND",
        new GateDesc2<logical_nand>("nand gate"));
    gateDescManager->AddDesc("GATE:NOR",
        new GateDesc2<logical_nor>("nor gate"));
    gateDescManager->AddDesc("GATE:XOR",
        new GateDesc2<logical_xor>("xor gate"));
  }
  return gateDescManager;
}

const DescManager*
GetFlipflopDescManager()
{
  static DescManager* flipflopDescManager = NULL;
  if(flipflopDescManager==NULL) {
    flipflopDescManager = new DescManager();
    // Unary
    flipflopDescManager->AddDesc("FLIPFLOP:D",
        new FlipflopDesc1<state_logical_d>("D flipflop"));
    flipflopDescManager->AddDesc("FLIPFLOP:T",
        new FlipflopDesc1<state_logical_t>("T flipflop"));
    // Binary
    flipflopDescManager->AddDesc("FLIPFLOP:JK",
        new FlipflopDesc2<state_logical_jk>("JK flipflop"));
    flipflopDescManager->AddDesc("FLIPFLOP:SR",
        new FlipflopDesc2<state_logical_sr>("SR flipflop"));
  }
  return flipflopDescManager;
}

} // namespace ak47

