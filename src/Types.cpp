#include "Types.h"

namespace ak47
{

std::vector<bool>
vectorBoolFromInt(size_t i, size_t size)
{
  std::vector<bool> result(size, false);
  size_t p = 0;
  while(i) {
    result[p++] = (bool)(i%2);
    i /= 2;
  }
  return result;
}

size_t
intFromVectorBool(const std::vector<bool>& vb)
{
  size_t result = 0;
  for(int i=vb.size()-1; i>=0; --i) {
    result *= 2;
    result += vb[i];
  }
  return result;

}
std::string
stringFromVectorBool(const std::vector<bool>& vb)
{
    std::string result;
    for(size_t i=0, maxi=vb.size(); i<maxi; ++i) {
        if(vb[i]) {
            result += "1";
        }
        else {
            result += "0";
        }
    }
    return result;
}

} // namespace ak47
