#include "EngineeringAlgo.h"

#include "Types.h"

namespace ak47
{

StateDiagram
ReverseEngineering(const CircuitBlock& cd)
{
  StateDiagram result;
  for(size_t i=0, maxi=1<<cd.GetStateBitNum(); i<maxi; ++i) {
    state_type state = vectorBoolFromInt(i, cd.GetStateBitNum());
    result.AddState(state);
  }
  for(size_t i=0, maxi=result.GetStateNum(); i<maxi; ++i) {
    for(size_t j=0, maxj=1<<cd.GetInputNum(); j<maxj; ++j) {
      state_type state = vectorBoolFromInt(i, cd.GetStateBitNum());
      input_type input = vectorBoolFromInt(j, cd.GetInputNum());
      result.SetNextState(
          state, input,
          cd.GetNextState(state, input),
          cd.GetOutput(state, input)
      );
    }
  }
  return result;
}

} // namespace ak47

