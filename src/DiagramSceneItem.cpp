#include "DiagramSceneItem.h"
#include "DiagramSceneLine.h"

#include <cstdio>

#include <QPainter>
#include <QTransform>
#include <QPen>
#include <QBrush>
#include <QGraphicsSceneMouseEvent>

namespace ak47{
  
  //
  // Todo: Refactor with Flyweight pattern
  //
  namespace{
    struct ItemMetaData{
      QString className;
      QString svgName;
      int numInputs;
      int numOutputs;
      QPointF inputs[2];
      QPointF outputs[1];
    };
    
    const ItemMetaData itemMetadata[] = {
      { "PORT:IN", ":/components/input.svg", 0, 1,
        {}, { QPointF(64, 32) }
      },
      
      { "PORT:OUT", ":/components/output.svg", 1, 0,
        { QPointF(0, 32) }, {}
      },
      
      { "GATE:AND", ":/components/and.svg", 2, 1,
        { QPointF(0, 20.76), QPointF(0, 43.24) }, { QPointF(96, 32) }
      },
      
      { "GATE:OR", ":/components/or.svg", 2, 1,
        { QPointF(0, 20.75), QPointF(0, 43.2) }, { QPointF(96, 32) }
      },
      
      { "GATE:NOT", ":/components/not.svg", 1, 1,
        { QPointF(0, 32) }, { QPointF(96, 32) }
      },
      
      { "FLIPFLOP:D", ":/components/d-ff.svg", 1, 1,
        { QPointF(0, 32.47) }, { QPointF(128, 32.47) }
      },
      
      { "FLIPFLOP:T", ":/components/t-ff.svg", 1, 1,
        { QPointF(0, 32.47) }, { QPointF(128, 32.47) }
      },
      
      { "FLIPFLOP:JK", ":/components/jk-ff.svg", 2, 1,
        { QPointF(0, 32.47), QPointF(0, 79.49) }, { QPointF(128, 32.47) }
      },
      
      { "FLIPFLOP:SR", ":/components/sr-ff.svg", 2, 1,
        { QPointF(0, 32.47), QPointF(0, 79.49) }, { QPointF(128, 32.47) }
      },
      
      { "GATE:NAND", ":/components/nand.svg", 2, 1,
        { QPointF(0, 20.76), QPointF(0, 43.24) }, { QPointF(96, 32) }
      },
      
      { "GATE:NOR", ":/components/nor.svg", 2, 1,
        { QPointF(0, 20.75), QPointF(0, 43.2) }, { QPointF(96, 32) }
      },
      
      { "GATE:XOR", ":/components/xor.svg", 2, 1,
        { QPointF(0, 20.75), QPointF(0, 43.2) }, { QPointF(96, 32) }
      },
      
      {
        "GATE:TRUE", ":/components/true.svg", 0, 1,
        {}, { QPointF(96, 32) }
      },
      
      {
        "GATE:FALSE", ":/components/false.svg", 0, 1,
        {}, { QPointF(96, 32) }
      }
      
    };
  }
  
  //
  
  DiagramSceneItem::DiagramSceneItem(QString _className)
  :className(_className)
  {
    //
    this->setFlag(QGraphicsItem::ItemIsSelectable, true);
    this->setFlag(QGraphicsItem::ItemIsMovable, true);
    
    //
    const ItemMetaData* meta = NULL;
    for(int i=0; i<sizeof(itemMetadata)/sizeof(itemMetadata[0]); ++i)
      if(itemMetadata[i].className == className)
        meta = itemMetadata + i;
    
    Q_CHECK_PTR(meta);
    
    //
    svg = new QSvgRenderer( meta->svgName , this);
    
    //
    numInputs = meta->numInputs;
    for(int i=0; i<numInputs; ++i)
      inputRegions[i] = QRectF(meta->inputs[i]-QPointF(0, 4), QSizeF(8, 8));
    
    //
    numOutputs = meta->numOutputs;
    for(int i=0; i<numOutputs; ++i)
      outputRegions[i] = QRectF(meta->outputs[i]-QPointF(8, 4), QSizeF(8, 8));
    
    //
    for(int i=0; i<numInputs; ++i)
      inputSource[i] = NULL;
    
  }
  
  DiagramSceneItem::~DiagramSceneItem()
  {
  }
  
  QString DiagramSceneItem::getClassName()const
  {
    return className;
  }
  
  QRectF DiagramSceneItem::boundingRect()const
  {
    return svg->viewBoxF();
  }
  
  void DiagramSceneItem::paint
  (QPainter *painter, const QStyleOptionGraphicsItem * option, QWidget *widget)
  {
    // World Matrix
    //QTransform oldWorld = painter->worldTransform();
    
    // Draw Main
    //painter->setWorldTransform(QTransform::fromTranslate(0, 0), true);
    svg->render(painter, svg->viewBoxF());
    
    // Draw Connectors
    painter->setPen(QPen(QBrush(Qt::transparent), 1));
    painter->setBrush(QBrush(Qt::red));
    for(int i=0; i<numInputs; ++i)
      painter->drawEllipse(inputRegions[i]);
    painter->setBrush(QBrush(Qt::green));
    for(int i=0; i<numOutputs; ++i)
      painter->drawEllipse(outputRegions[i]);
    
    // Draw Selection Bound
    if(this->isSelected()){
      painter->setBrush(QBrush(Qt::transparent));
      painter->setPen(QPen(QBrush(Qt::black), 2, Qt::DashLine));
      painter->drawRect(this->boundingRect());
    }
    
    // Reset World Transform
    //painter->setWorldTransform(oldWorld, false);
    
  }
  
  QRectF DiagramSceneItem::inputConnRect(int index)const
  {
    return inputRegions[index];
  }
  
  QRectF DiagramSceneItem::outputConnRect(int index)const
  {
    return outputRegions[index];
  }
  
  bool DiagramSceneItem::_registerWire(DiagramSceneLine* wire)
  {
    if(wire->inputItemPtr() == this){
      int index = wire->inputIndex();
      
      // check
      if(inputSource[index] != NULL)
        return false;
      
      //
      inputSource[index] = wire;
      
      //
      connect((QObject*)wire, SIGNAL(destroyed(QObject*)), 
              (QObject*)this, SLOT(onInputConnDestroyed(QObject*)));
      
    }else
      return false;
    return true;
  }
  
  void DiagramSceneItem::onInputConnDestroyed(QObject* obj)
  {
    for(int i=0; i<numInputs; ++i)
      if((QObject*)inputSource[i] == obj){
        inputSource[i] = NULL;
      }
  }
  
  void DiagramSceneItem::mousePressEvent(QGraphicsSceneMouseEvent * event)
  {
    QPointF pos = this->mapFromScene(event->scenePos()); // item space
    
    for(int i=0; i<numInputs; ++i)
      if(inputRegions[i].contains(pos)){
        emit inputPressed(this, i);
        //return;
      }
    for(int i=0; i<numOutputs; ++i)
      if(outputRegions[i].contains(pos)){
        emit outputPressed(this, i);
        //return;
      }
    
    if(_defaultMouseEvent())
      QGraphicsObject::mousePressEvent(event);
  }
  
  void DiagramSceneItem::mouseReleaseEvent(QGraphicsSceneMouseEvent * event)
  {
    QPointF pos = this->mapFromScene(event->scenePos()); // item space
    
    //std::printf("DiagramSceneItem %p: mouse release, "
    //            "pos = (%f, %f)\n", this, pos.x(),pos.y());
    
    for(int i=0; i<numInputs; ++i)
      if(inputRegions[i].contains(pos)){
        emit inputReleased(this, i);
        //return;
      }
    for(int i=0; i<numOutputs; ++i)
      if(outputRegions[i].contains(pos)){
        emit outputReleased(this, i);
        //return;
      }
    
    if(_defaultMouseEvent())
      QGraphicsObject::mouseReleaseEvent(event);
  }
  
  void DiagramSceneItem::mouseMoveEvent(QGraphicsSceneMouseEvent * event)
  {
    if(_defaultMouseEvent())
      QGraphicsObject::mouseMoveEvent(event);
  }
  
  ////////////////////////////
  
  bool DiagramSceneItem::_defaultMouseEvent()
  {
    return _bDefaultMouseEvent;
  }
  
  void DiagramSceneItem::_setDefaultMouseEvent(bool enabled)
  {
    _bDefaultMouseEvent = enabled;
  }
  
  bool DiagramSceneItem::_bDefaultMouseEvent = true;
  
}
