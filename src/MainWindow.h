#ifndef AK47__MAINWINDOW_H
#define AK47__MAINWINDOW_H

// Qt Lib
#include <QMainWindow>
#include <QActionGroup>

// qmake generated file
#include "ui_MainWindow.h"

// ak47
#include "DiagramScene.h"
#include "DiagramSceneModel.h"

namespace ak47{
  
  class MainWindow : public QMainWindow, private Ui_MainWindow 
  {
    Q_OBJECT
  public:
    MainWindow();
    virtual ~MainWindow();
    
  private slots:
    void on_actionMove_toggled(bool checked);
    void on_actionWire_toggled(bool checked);
    void on_actionInput_toggled(bool checked);
    void on_actionOutput_toggled(bool checked);
    void on_actionAND_Gate_toggled(bool checked);
    void on_actionOR_Gate_toggled(bool checked);
    void on_actionNOT_Gate_toggled(bool checked);
    void on_actionD_Flip_Flop_toggled(bool checked);
    void on_actionT_Flip_Flop_toggled(bool checked);
    void on_actionJK_Flip_Flop_toggled(bool checked);
    void on_actionSR_Flip_Flop_toggled(bool checked);
    void on_actionBuild_State_Diagram_triggered();
    void on_actionNAND_Gate_toggled(bool checked);
    void on_actionNOR_Gate_toggled(bool checked);
    void on_actionXOR_Gate_toggled(bool checked);
    void on_actionTRUE_Gate_toggled(bool checked);
    void on_actionFALSE_Gate_toggled(bool checked);
    void on_actionAbout_triggered();
    
  private:
    QActionGroup* paletteActionGroup;
    DiagramScene* scene;
    DiagramSceneModel* model;
    
  };
  
}

#endif // AK47__MAINWINDOW_H

