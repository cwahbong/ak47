#include "StateDiagramSceneState.h"

#include <QPainter>

namespace ak47
{

StateDiagramSceneState::StateDiagramSceneState(
    const state_type& state, QGraphicsObject *parent)
    : QGraphicsObject(parent)
{
    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    text.append(stringFromVectorBool(state).c_str());
}

QRectF
StateDiagramSceneState::boundingRect() const
{
    return QRectF(0, 0, 50, 50);
}

void
StateDiagramSceneState::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
{
  painter->setBrush(QBrush(Qt::white));
    painter->drawEllipse(0, 0, 50, 50);
    painter->drawText(10, 30, text);
    if(isSelected()) {
        painter->setBrush(QBrush(Qt::transparent));
        painter->setPen(QPen(QBrush(Qt::black), 2, Qt::DashLine));
        painter->drawRect(this->boundingRect());
    }
}

} // namespace ak47

