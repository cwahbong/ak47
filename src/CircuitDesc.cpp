#include "CircuitDesc.h"

namespace ak47
{

CircuitDesc::CircuitDesc(
    size_t inputNum,
    size_t outputNum,
    size_t stateBitNum,
    const std::string& name
): _inputNum(inputNum),
  _outputNum(outputNum),
  _stateBitNum(stateBitNum),
  _name(name)
{
}

size_t
CircuitDesc::GetInputNum() const
{
  return _inputNum;
}

size_t
CircuitDesc::GetOutputNum() const
{
  return _outputNum;
}

size_t
CircuitDesc::GetStateBitNum() const
{
  return _stateBitNum;
}

std::string
CircuitDesc::GetName() const
{
  return _name;
}

} // namespace ak47

