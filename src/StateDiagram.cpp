#include "StateDiagram.h"

#include <cstdio>
#include <utility>

namespace ak47
{

bool
StateDiagram::AddState(state_type state)
{
  if(state==STATE_INVALID) {
    return false;
  }
  if(_statesMap.insert(
        std::make_pair(state, edges_type())
        ).second) {
    _statesList.push_back(state);
    return true;
  }
  else {
    return false;
  }
}

size_t
StateDiagram::GetStateNum() const
{
  return _statesMap.size();
}

std::vector<state_type>
StateDiagram::GetStates() const
{
  return _statesList;
}

state_type
StateDiagram::GetNextState(state_type state, input_type input) const
{
  if(_statesMap.find(state)==_statesMap.end()) {
    return STATE_INVALID;
  }
  const edges_type& nextmap = _statesMap.find(state)->second;
  if(nextmap.find(input)==nextmap.end()) {
    return STATE_INVALID;
  }
  return nextmap.find(input)->second.first;
}

StateDiagram::edges_type
StateDiagram::GetEdges(state_type state) const
{
    if(_statesMap.find(state)==_statesMap.end()) {
        return edges_type();
    }
    return _statesMap.find(state)->second;
}

output_type
StateDiagram::GetOutput(state_type state, input_type input) const
{
    if(_statesMap.find(state)==_statesMap.end()) {
        return STATE_INVALID;
    }
    const edges_type& nextmap = _statesMap.find(state)->second;
    if(nextmap.find(input)==nextmap.end()) {
        return STATE_INVALID;
    }
    return nextmap.find(input)->second.second;
}

bool
StateDiagram::SetNextState(state_type state, input_type edge, state_type next, output_type output)
{
  if(_statesMap.find(state)==_statesMap.end()) {
    return false;
  }
  edges_type& nextmap = _statesMap.find(state)->second;
  return nextmap.insert(std::make_pair(edge, std::make_pair(next, output))).second;
}

} // namespace ak47

