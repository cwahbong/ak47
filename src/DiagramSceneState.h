#ifndef AK47__DIAGRAMSCENESTATE_H
#define AK47__DIAGRAMSCENESTATE_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QGraphicsLineItem>

namespace ak47{
  
  class DiagramSceneLine;
  class DiagramSceneItem;
  class DiagramSceneModel;
  
  class DiagramSceneState : public QObject{
    Q_OBJECT
  protected:
    DiagramSceneState(QGraphicsScene* parent, DiagramSceneModel* model);
  public:
    virtual ~DiagramSceneState();
    
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent* event)=0;
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent* event)=0;
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent* event)=0;
    
  protected:
    QGraphicsScene* parentScene;
    DiagramSceneModel* parentModel;
    
  };
  
  ////////////////////////////////////
  
  class DiagramSceneMoving : public DiagramSceneState{
    Q_OBJECT
  public:
    DiagramSceneMoving(QGraphicsScene* parent, DiagramSceneModel* model);
    virtual ~DiagramSceneMoving();
    
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent* event);
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    
  private:
    
  };
  
  ////////////////////////////////////
  
  class DiagramSceneAddLine : public DiagramSceneState{
    Q_OBJECT
  public:
    DiagramSceneAddLine(QGraphicsScene* parent, DiagramSceneModel* model);
    virtual ~DiagramSceneAddLine();
    
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent* event);
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    
  private slots:
    void startOnInputConn(QGraphicsObject* sender, int index);
    void endOnInputConn(QGraphicsObject* sender, int index);
    void startOnOutputConn(QGraphicsObject* sender, int index);
    void endOnOutputConn(QGraphicsObject* sender, int index);
    
  private:
    void cancelTracking();
    void addLine(QGraphicsObject* input, int inIndex,
                 QGraphicsObject* output, int outIndex);
    
    QGraphicsObject *inputStart, *outputStart;
    int inputStartIndex, outputStartIndex;
    
    DiagramSceneLine *tempLine;
  };
  
  ////////////////////////////////////
  
  class DiagramSceneAddItem : public DiagramSceneState{
    Q_OBJECT
  public:
    DiagramSceneAddItem(QString className, 
                        QGraphicsScene* parent, DiagramSceneModel* model);
    virtual ~DiagramSceneAddItem();
    
    virtual bool mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    virtual bool mousePressEvent(QGraphicsSceneMouseEvent* event);
    virtual bool mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    
  private:
    QString itemClass;
    QGraphicsItem* itemObj;
    
  };
  
}

#endif // AK47__DIAGRAMSCENESTATE_H
