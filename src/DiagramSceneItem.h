#ifndef AK47__DIAGRAMSCENEITEM_H
#define AK47__DIAGRAMSCENEITEM_H

#include <QString>
#include <QGraphicsObject>
#include <QSvgRenderer>
#include <QRect>

namespace ak47{
  
  class DiagramSceneLine;
  
  class DiagramSceneItem : public QGraphicsObject
  {
    Q_OBJECT
  public:
    DiagramSceneItem(QString _className);
    virtual ~DiagramSceneItem();
    
    QString getClassName()const;
    
    virtual QRectF boundingRect()const;
    virtual void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
    
    static bool _defaultMouseEvent();
    static void _setDefaultMouseEvent(bool enabled);
    
    QRectF inputConnRect(int index)const;
    QRectF outputConnRect(int index)const;
    
    bool _registerWire(DiagramSceneLine* wire);
    
  private slots:
    void onInputConnDestroyed(QObject*);
    
  signals:
    void inputPressed(QGraphicsObject* sender, int index);
    void inputReleased(QGraphicsObject* sender, int index);
    void outputPressed(QGraphicsObject* sender, int index);
    void outputReleased(QGraphicsObject* sender, int index);
    
  private:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent * event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    
  private:
    //
    // Todo: Refactor with Flyweight pattern
    //
    QString className;
    
    //
    // Todo: Refactor with Flyweight pattern
    //
    QSvgRenderer* svg;
    
    //
    // Todo: Refactor with Flyweight pattern
    //
    int numInputs;
    QRectF inputRegions[2];
    
    //
    // Todo: Refactor with Flyweight pattern
    //
    int numOutputs;
    QRectF outputRegions[1];
    
    //
    const DiagramSceneLine* inputSource[2];
    
  private:
    // todo: refactor it!
    static bool _bDefaultMouseEvent;
    
  };
  
}

#endif // AK47__DIAGRAMSCENEITEM_H
