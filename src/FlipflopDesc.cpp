#include "FlipflopDesc.h"

namespace ak47
{

state_type
FlipflopDesc::GetNextState(state_type state, input_type input) const
{
  return GetOutput(state, input);
}

} // namespace ak47

