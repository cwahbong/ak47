#ifndef AK47__CIRCUITCOMPONENT_H
#define AK47__CIRCUITCOMPONENT_H

#include "Types.h"

#include <set>
#include <string>
#include <vector>

namespace ak47
{

/**
 * \brief Represents a component in a circuit.
 *
 * This is the base class of all component of a circuit, and it should
 * not be used directly.
 */
class CircuitComponent
{
private:
  typedef std::pair<id_type, id_type> id_pair;
  struct cmp_pair{
    bool operator()(const id_pair& lhs, const id_pair& rhs) {
      return lhs.second <= rhs.first;
    }
  };
  /**
   * use [from, to) pairs to record the used id.
   */
  static std::set<id_pair, cmp_pair> _usedId;
  static void AddUsedId(id_type id);
  static void RemoveUsedId(id_type id);
  static bool IsUsed(id_type id);

protected:
  static id_type GetNewId(id_type id=ID_DEFAULT);

private:
  const id_type _id;

protected:
  CircuitComponent(id_type id=ID_DEFAULT);

public:
  virtual ~CircuitComponent();
  
  /// Get the number of input.
  virtual size_t GetInputNum() const = 0;
  /// Get the number of output.
  virtual size_t GetOutputNum() const = 0;
  /// Get the number of state bit.
  virtual size_t GetStateBitNum() const = 0;
  /// Get the name of the component.
  virtual std::string GetName() const = 0;
  /// Get the id of the component.
  id_type GetId() const;

  /// Get the output by the specific state and input.
  virtual std::vector<bool> GetOutput(state_type state, input_type input) const = 0;
  /// Get the next state by the specific state and input.
  virtual state_type GetNextState(state_type state, input_type input) const = 0;
};

} // namespace ak47
#endif // AK47__CIRCUITCOMPONENT_H

