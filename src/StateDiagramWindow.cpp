#include "StateDiagramWindow.h"
#include "ui_StateDiagramWindow.h"

namespace ak47
{

StateDiagramWindow::StateDiagramWindow(QWidget *parent, const StateDiagram* sd) :
    QDialog(parent)
{
    setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);
    stateDiagramView->setScene(new StateDiagramScene(this, sd));
}

StateDiagramWindow::~StateDiagramWindow()
{
}

} // namespace ak47
