#ifndef AK47__CIRCUITBLOCK_H
#define AK47__CIRCUITBLOCK_H

#include "CircuitComponent.h"

#include <map>
#include <vector>

namespace ak47
{

class CircuitBlock: public CircuitComponent
{
private:
  struct AccumulateNextState;

public:
  typedef std::pair<id_type, size_type> side_type;
private:
  const size_t _inputNum;
  const size_t _outputNum;
  const std::string _name;
  const std::map<id_type, const CircuitComponent*> _comps;
  const std::map<side_type, side_type> _compsWireFrom;
  /**
   * prevent copy and assignment, will not implement in cpp
   */
  CircuitBlock(const CircuitBlock&);
  CircuitBlock& operator=(const CircuitBlock& rhs);
  /**
   */
  state_type GetComponentState(id_type id, state_type state) const;
  input_type GetComponentInput(id_type id, state_type state, input_type input) const;
  /**
   */
  output_type GetComponentOutput(id_type id, state_type state, input_type input) const;
  /**
   */
  state_type GetComponentNextState(id_type id, state_type state, input_type input) const;
  const CircuitComponent* GetComponentById(id_type id) const;

public:
  CircuitBlock(
      size_type inputNum,
      size_type outputNum,
      const std::string& name,
      std::map<id_type, const CircuitComponent*> comps,
      std::map<side_type, side_type> compsWireFrom,
      id_type id = ID_DEFAULT
  );

  virtual ~CircuitBlock();

  virtual size_t GetInputNum() const;
  virtual size_t GetOutputNum() const;
  virtual size_t GetStateBitNum() const;
  virtual std::string GetName() const;

  virtual std::vector<bool> GetOutput(state_type state, input_type input) const;
  virtual state_type GetNextState(state_type state, input_type input) const;

};

} // namespace ak47
#endif // AK47__CIRCUITBLOCK_H

