#include "CircuitComponent.h"

#include <cassert>

namespace ak47
{

std::set<CircuitComponent::id_pair, CircuitComponent::cmp_pair>
CircuitComponent::_usedId;

void
CircuitComponent::AddUsedId(id_type id)
{
  assert(IsUsed(id)==false);
  id_pair to_insert(id, id+1);
  std::set<id_pair, cmp_pair>::iterator
  prev = _usedId.find(id_pair(id-1, id)),
  next = _usedId.find(id_pair(id+1, id+2));
  if(prev!=_usedId.end()) {
    id_type first = prev->first;
    _usedId.erase(prev);
    to_insert.first = first;
  }
  if(next!=_usedId.end()) {
    id_type second = next->second;
    _usedId.erase(next);
    to_insert.second = second;
  }
  _usedId.insert(to_insert);
}

void
CircuitComponent::RemoveUsedId(id_type id)
{
  assert(IsUsed(id)==true);
  std::set<id_pair, cmp_pair>::iterator
  current = _usedId.find(id_pair(id, id+1));
  //fprintf(stderr, "%d    %d %d\n", id, current->first, current->second);
  _usedId.erase(current);
  if(id>current->first) {
    _usedId.insert(id_pair(current->first, id));
  }
  if(id+1<current->second) {
    _usedId.insert(id_pair(id+1, current->second));
  }
}

bool
CircuitComponent::IsUsed(id_type id)
{
  return _usedId.find(id_pair(id, id+1))!=_usedId.end();
}

id_type
CircuitComponent::GetNewId(id_type id)
{
  if(id==ID_DEFAULT || IsUsed(id)) {
    if(_usedId.empty() || _usedId.begin()->first>0) {
      return 0;
    }
    else {
      return _usedId.begin()->second;
    }
  }
  return id;
}

CircuitComponent::CircuitComponent(id_type id)
  :_id(GetNewId(id))
{
  AddUsedId(_id);
}

CircuitComponent::~CircuitComponent()
{
  RemoveUsedId(_id);
}

id_type
CircuitComponent::GetId() const
{
  return _id;
}

} // namespace ak47

