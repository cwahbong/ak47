#include "DiagramSceneModel.h"
#include "DiagramSceneLine.h"
#include "DiagramSceneItem.h"
#include "EngineeringAlgo.h"

#include <cstdio>
#include <cstdlib>
#include <string>

namespace ak47{
  
  DiagramSceneModel::DiagramSceneModel(QObject* parent)
  :QObject(parent)
  {
    creator = new CircuitCreator();
  }
  
  DiagramSceneModel::~DiagramSceneModel()
  {
    delete creator;
  }
  
  StateDiagram DiagramSceneModel::getStateDiagram()
  {
    CircuitBlockBuilder builder;
    
    QMap<QObject*, id_type> componentTable;
    QMap<QObject*, int> blockInputTable;
    QMap<QObject*, int> blockOutputTable;
    
    {
      foreach(QObject* obj, componentSet){
        QString className = ((DiagramSceneItem*)obj)->getClassName();
        std::string type = className.toStdString();
        
        const CircuitComponent* comp = creator->NewCircuitComponent(type);
        if(comp == NULL)
          std::fprintf(stderr, "cannot create new component\n");
        
        componentTable.insert(obj, comp->GetId());
        builder.AddComponent(comp);
      }
    }
    {
      int i;
      
      i = 0;
      foreach(QObject* obj, blockInputList){
        blockInputTable.insert(obj, i++);
      }
      
      i = 0;
      foreach(QObject* obj, blockOutputList){
        blockOutputTable.insert(obj, i++);
      }
      
      builder.SetInputNum(blockInputTable.size());
      builder.SetOutputNum(blockOutputTable.size());
    }
    {
      QSet<QObject*>::iterator it, itend;
      it = connectionSet.begin();
      itend = connectionSet.end();
      for(; it != itend; ++it){
        DiagramSceneLine* wire = (DiagramSceneLine*)*it;
        
        const DiagramSceneItem* from = wire->outputItemPtr();
        int fromIndex = wire->outputIndex();
        id_type fromId = getId(from, componentTable);
        if(fromId == ID_PARENT)
          fromIndex = blockInputTable.value((QObject*)from);
        
        const DiagramSceneItem* to = wire->inputItemPtr();
        int toIndex = wire->inputIndex();
        id_type toId = getId(to, componentTable);
        if(toId == ID_PARENT)
          toIndex = blockOutputTable.value((QObject*)to);
        
        builder.Connect(side_type(fromId, fromIndex),
                        side_type(toId, toIndex));
      }
    }
    
    const CircuitBlock* block = creator->NewCircuitBlock(builder);
    if(block == NULL){
      // todo: 
      std::abort();
    }
    
    StateDiagram sd = ReverseEngineering(*block);
    
    delete block;
    
    return sd; // it will be optimized away with RVO
  }
  
  void DiagramSceneModel::addItem(DiagramSceneItem* obj)
  {
    //
    const std::string type = obj->getClassName().toStdString();
    
    if(isComponent(type)){
      //componentSet.insert((QObject*)obj);
      componentSet.append((QObject*)obj);
    }else if(isInputPort(type)){
      blockInputList.append((QObject*)obj);
      
    }else if(isOutputPort(type)){
      blockOutputList.append((QObject*)obj);
      
    }else{
      std::fprintf(stderr, "unknown type.\n");
      std::abort();
    }
    
    //
    connect((QObject*)obj, SIGNAL(destroyed(QObject*)),
            (QObject*)this, SLOT(onItemDestroyed(QObject*)));
    
    //
    std::printf("debug: add item(%p) as %s\n", obj, type.c_str());
    
  }
  
  void DiagramSceneModel::addWire(DiagramSceneLine* obj)
  {
    //std::printf("Wire %p is added\n", obj);
    
    connectionSet.insert((QObject*)obj);
    
    connect((QObject*)obj, SIGNAL(destroyed(QObject*)),
            (QObject*)this, SLOT(onWireDestroyed(QObject*)));
    
    std::printf("debug: add wire(%p): (%p, %d)->(%p, %d)\n",
                obj,
                obj->outputItemPtr(), obj->outputIndex(),
                obj->inputItemPtr(), obj->inputIndex());
  }
  
  void DiagramSceneModel::onItemDestroyed(QObject* obj)
  {
    //componentSet.remove(obj);
    componentSet.removeAll(obj);
    blockInputList.removeAll(obj);
    blockOutputList.removeAll(obj);
    
    std::printf("debug: remove item(%p)\n", obj);
  }
  
  void DiagramSceneModel::onWireDestroyed(QObject* obj)
  {
    connectionSet.remove(obj);
    
    std::printf("debug: remove wire(%p)\n", obj);
  }
  
  bool DiagramSceneModel::isInputPort(const std::string& type)
  {
    return type == "PORT:IN";
  }
  
  bool DiagramSceneModel::isOutputPort(const std::string& type)
  {
    return type == "PORT:OUT";
  }
  
  bool DiagramSceneModel::isComponent(const std::string& type)
  {
    return (type.compare(0, 5, "GATE:") == 0 ||
            type.compare(0, 9, "FLIPFLOP:") == 0);
  }
  
  id_type DiagramSceneModel::getId
  (const DiagramSceneItem* item, const QMap<QObject*, id_type>& comp)
  {
    QObject* obj = (QObject*)item;
    
    id_type idx = comp.value(obj, -1);
    if(idx != -1)
      return idx;
    
    return ID_PARENT;
  }
  
}
