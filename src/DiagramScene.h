#ifndef AK47__DIAGRAMSCENE_H
#define AK47__DIAGRAMSCENE_H

// Qt Lib
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QAbstractGraphicsShapeItem>

// ak47
#include "DiagramSceneState.h"
#include "DiagramSceneModel.h"

namespace ak47{
  
  class DiagramScene : public QGraphicsScene
  {
    Q_OBJECT
    
  public:
    enum Mode{ MOVE = 0, ADDLINE, ADDITEM };
    
  public:
    DiagramScene(DiagramSceneModel* _model, QObject* parent);
    virtual ~DiagramScene();
    
    void setMode(Mode mode, QString itemClass = "");
    
  private:
    DiagramSceneState* state;
    DiagramSceneModel* model;
    
  private slots:
    void onItemSuicide(QGraphicsItem* item);
    
  private:
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* mouseEvent);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* mouseEvent);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent);
    virtual void keyReleaseEvent(QKeyEvent* keyEvent);
    
  };
  
}

#endif // AK47__DIAGRAMSCENE_H
