#ifndef AK47__STATEDIAGRAMWINDOW_H
#define AK47__STATEDIAGRAMWINDOW_H

#include <QDialog>

#include "ui_StateDiagramWindow.h"

#include "StateDiagram.h"
#include "StateDiagramScene.h"

namespace ak47
{

namespace Ui {
    class StateDiagramWindow;
}

class StateDiagramWindow : public QDialog, private Ui_StateDiagramWindow
{
    Q_OBJECT

public:
    explicit StateDiagramWindow(QWidget *parent = 0, const StateDiagram* sd = 0);
    ~StateDiagramWindow();

};

} // namespace ak47
#endif // AK47__STATEDIAGRAMWINDOW_H
