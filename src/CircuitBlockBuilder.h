#ifndef AK47__CIRCUITBLOCKBUILDER_H
#define AK47__CIRCUITBLOCKBUILDER_H

#include "CircuitBlock.h"
#include "CircuitComponent.h"

namespace ak47
{

class CircuitBlockBuilder
{
public:
  typedef CircuitBlock::side_type side_type;

private:
  size_type _inputNum;
  size_type _outputNum;
  std::string _name;
  std::map<id_type, const CircuitComponent*> _comps;
  std::map<side_type, side_type> _compsWireFrom;
public:
  void SetInputNum(size_type inputNum);
  void SetOutputNum(size_type outputNum);
  void SetName(const std::string& name);
  void AddComponent(const CircuitComponent* component);
  void Connect(side_type out, side_type in);
  bool Validate() const;
  const CircuitBlock* Build() const;
};

} // namespace ak47
#endif // AK47__CIRCUITBLOCKBUILDER_H

