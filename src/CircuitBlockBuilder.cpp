#include "CircuitBlockBuilder.h"

namespace ak47
{

void
CircuitBlockBuilder::SetInputNum(size_type inputNum)
{
  _inputNum = inputNum;
}

void
CircuitBlockBuilder::SetOutputNum(size_type outputNum)
{
  _outputNum = outputNum;
}

void
CircuitBlockBuilder::SetName(const std::string& name)
{
  _name = name;
}

void
CircuitBlockBuilder::AddComponent(const CircuitComponent* component)
{
  _comps[component->GetId()] = component;
}

void
CircuitBlockBuilder::Connect(side_type out, side_type in)
{
  _compsWireFrom[in] = out;
}

bool
CircuitBlockBuilder::Validate() const
{
  // TODO safe check
  return true;
}

const CircuitBlock*
CircuitBlockBuilder::Build() const
{
  return new CircuitBlock(_inputNum, _outputNum, _name, _comps, _compsWireFrom);
}

} // namespace ak47

