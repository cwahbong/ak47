#include "DiagramSceneLine.h"
#include "DiagramSceneItem.h"
#include <QPen>
#include <QBrush>
#include <QPainter>

#include <algorithm>
#include <cmath>

namespace ak47{
  
  DiagramSceneLine::DiagramSceneLine()
  {
    inputItem = NULL;
    outputItem = NULL;
    
    this->setFlag(QGraphicsItem::ItemIsSelectable, true);
  }
  
  DiagramSceneLine::~DiagramSceneLine()
  {
  }
  
  bool DiagramSceneLine::setConnectors
  (DiagramSceneItem* _inputItem, int _inputIndex,
   DiagramSceneItem* _outputItem, int _outputIndex)
  {
    Q_ASSERT( inputItem == NULL && outputItem == NULL );
    
    //
    inputItem = _inputItem;
    inputConnectorIndex = _inputIndex;
    
    //
    outputItem = _outputItem;
    outputConnectorIndex = _outputIndex;
    
    //
    if(!_inputItem->_registerWire(this))
      return false;
    
    //
    connect((QObject*)inputItem, SIGNAL(xChanged()), 
            (QObject*)this, SLOT(inputMoved()));
    connect((QObject*)inputItem, SIGNAL(yChanged()), 
            (QObject*)this, SLOT(inputMoved()));
    connect((QObject*)inputItem, SIGNAL(zChanged()), 
            (QObject*)this, SLOT(inputMoved()));
    connect((QObject*)inputItem, SIGNAL(destroyed(QObject*)),
            (QObject*)this, SLOT(inputRemoved()));
    
    //
    connect((QObject*)outputItem, SIGNAL(xChanged()), 
            (QObject*)this, SLOT(outputMoved()));
    connect((QObject*)outputItem, SIGNAL(yChanged()), 
            (QObject*)this, SLOT(outputMoved()));
    connect((QObject*)outputItem, SIGNAL(zChanged()), 
            (QObject*)this, SLOT(outputMoved()));
    connect((QObject*)outputItem, SIGNAL(destroyed(QObject*)),
            (QObject*)this, SLOT(outputRemoved()));
    
    //
    inputMoved();
    outputMoved();
    
    return true;
  }
  
  void DiagramSceneLine::setInputPoint(QPointF pt)
  {
    inputPt = pt;
    updatePath();
    this->update();
  }
  
  void DiagramSceneLine::setOutputPoint(QPointF pt)
  {
    outputPt = pt;
    updatePath();
    this->update();
  }
  
  QRectF DiagramSceneLine::boundingRect()const
  {
    return path.boundingRect();
  }
  
  QPainterPath DiagramSceneLine::shape()const
  {
    return path;
  }
  
  void DiagramSceneLine::paint
  (QPainter* painter, const QStyleOptionGraphicsItem* style, QWidget* wdgt)
  {
    QBrush brush;
    
    if(this->isSelected())
      brush = QBrush(Qt::blue);
    else
      brush = QBrush(Qt::black);
    
    painter->setPen(QPen(brush, 1));
    painter->setBrush(brush);
    
    painter->drawPath(path);
  }
  
  int DiagramSceneLine::outputIndex()const
  {
    return outputConnectorIndex;
  }
  
  int DiagramSceneLine::inputIndex()const
  {
    return inputConnectorIndex;
  }
  
  const DiagramSceneItem* DiagramSceneLine::inputItemPtr()const
  {
    return inputItem;
  }
  
  const DiagramSceneItem* DiagramSceneLine::outputItemPtr()const
  {
    return outputItem;
  }
  
  void DiagramSceneLine::inputMoved()
  {
    Q_CHECK_PTR(inputItem);
    
    QRectF rt = inputItem->inputConnRect(inputConnectorIndex);
    
    inputPt = inputItem->mapToScene(rt.center());
    
    updatePath();
    this->update();
  }
  
  void DiagramSceneLine::outputMoved()
  {
    Q_CHECK_PTR(outputItem);
    
    QRectF rt = outputItem->outputConnRect(outputConnectorIndex);
    
    outputPt = outputItem->mapToScene(rt.center());
    
    updatePath();
    this->update();
  }
  
  void DiagramSceneLine::inputRemoved()
  {
    Q_CHECK_PTR(inputItem);
    
    /*
    disconnect((QObject*)inputItem, SIGNAL(xChanged()), 
               (QObject*)this, SLOT(inputMoved()));
    disconnect((QObject*)inputItem, SIGNAL(yChanged()), 
               (QObject*)this, SLOT(inputMoved()));
    disconnect((QObject*)inputItem, SIGNAL(zChanged()), 
               (QObject*)this, SLOT(inputMoved()));
    disconnect((QObject*)inputItem, SIGNAL(destroyed(QObject*)),
               (QObject*)this, SLOT(inputRemoved()));
    */
    
    inputItem = NULL;
    
    emit suicide(this);
  }
  
  void DiagramSceneLine::outputRemoved()
  {
    Q_CHECK_PTR(outputItem);
    
    /*
    disconnect((QObject*)outputItem, SIGNAL(xChanged()), 
               (QObject*)this, SLOT(outputMoved()));
    disconnect((QObject*)outputItem, SIGNAL(yChanged()), 
               (QObject*)this, SLOT(outputMoved()));
    disconnect((QObject*)outputItem, SIGNAL(zChanged()), 
               (QObject*)this, SLOT(outputMoved()));
    disconnect((QObject*)outputItem, SIGNAL(destroyed(QObject*)),
               (QObject*)this, SLOT(outputRemoved()));
    */
    
    outputItem = NULL;
    
    emit suicide(this);
  }
  
  void DiagramSceneLine::updatePath()
  {
    // notify the scene that the geometry is to be changed
    this->prepareGeometryChange();
    
    // reset path
    path = QPainterPath();
    
    // move the origin
    this->setPos(inputPt);
    
    QPointF pt = this->mapFromScene(outputPt);
    QPointF ptMid = pt / 2;
    
    QPolygonF polygon;
    
    polygon << QPointF(0, 0); // origin
    
    if(pt.x() < 0){
      polygon << QPointF(ptMid.x(), 0);
      polygon << QPointF(ptMid.x(), pt.y());
    }else if(inputItem && outputItem){
      QRectF inputRt = inputItem->mapRectToScene(inputItem->boundingRect());
      inputRt = this->mapRectFromScene(inputRt);
      QRectF outputRt = outputItem->mapRectToScene(outputItem->boundingRect());
      outputRt = this->mapRectFromScene(outputRt);
      
      const qreal yi1 = inputRt.top(), yi2 = inputRt.bottom();
      const qreal yo1 = outputRt.top(), yo2 = outputRt.bottom();
      qreal my = ptMid.y();
      
      if(yi1 < yo2 && yo1 < yi2){
        // intersected!
        qreal y1 = std::min(yi1, yo1);
        qreal y2 = std::max(yi2, yo2);
        
        if(pt.y() < 0)
          my = y2;
        else
          my = y1;
        
      }else if((yi1 < my && yi2 > my) || (yo1 < my && yo2 > my)){
        //
        if(yi2 < yo1){
          if(std::abs(my - yi2) < std::abs(my - yo1))
            my = yi2;
          else
            my = yo1;
        }else{
          if(std::abs(my - yo2) < std::abs(my - yi1))
            my = yo2;
          else
            my = yi1;
        }
        
      }
      
      polygon << QPointF(0, my);
      polygon << QPointF(pt.x(), my);
      
    }else{
      polygon << QPointF(0, ptMid.y());
      polygon << QPointF(pt.x(), ptMid.y());
      
    }
    
    polygon << pt;
    
    //
    for(int y=-1; y<=1; y++)
      for(int x=-1; x<=1; x++){
        if(x == 0 || y == 0)
          continue;
        
        QPolygonF composedPolygon = polygon;
        for(int i=0; i<polygon.size(); ++i)
          composedPolygon << polygon[polygon.size()-1-i] + QPointF(x,y);
        
        path.addPolygon(composedPolygon);
      }
    
  }
  
  void DiagramSceneLine::mousePressEvent(QGraphicsSceneMouseEvent * event)
  {
    if(_defaultMouseEvent())
      QGraphicsObject::mousePressEvent(event);
  }
  
  void DiagramSceneLine::mouseReleaseEvent(QGraphicsSceneMouseEvent * event)
  {
    if(_defaultMouseEvent())
      QGraphicsObject::mouseReleaseEvent(event);
  }
  
  void DiagramSceneLine::mouseMoveEvent(QGraphicsSceneMouseEvent * event)
  {
    if(_defaultMouseEvent())
      QGraphicsObject::mouseMoveEvent(event);
  }
  
  bool DiagramSceneLine::_defaultMouseEvent()
  {
    return _bDefaultMouseEvent;
  }
  
  void DiagramSceneLine::_setDefaultMouseEvent(bool enable)
  {
    _bDefaultMouseEvent = enable;
  }
  
  bool DiagramSceneLine::_bDefaultMouseEvent = true;
  
}
