#ifndef AK47__DESCMANAGER_H
#define AK47__DESCMANAGER_H

#include "CircuitDesc.h"

#include <map>

namespace ak47
{

class DescManager
{
private:
  std::map<std::string, const CircuitDesc*> _coreMap;
public:
  void AddDesc(const std::string& name, const CircuitDesc* core);
  const CircuitDesc* GetDesc(const std::string& name) const;
};

const DescManager* GetGateDescManager();
const DescManager* GetFlipflopDescManager();

} // namespace ak47
#endif // AK47__DESCMANAGER_H

