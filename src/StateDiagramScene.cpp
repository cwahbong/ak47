#include "StateDiagramScene.h"

#include "StateDiagramSceneState.h"

#include <cstdio>

#include <QMap>

namespace ak47
{

StateDiagramScene::StateDiagramScene(QObject *parent, const StateDiagram* sd) :
    QGraphicsScene(parent)
{
    if(sd) {
        setSceneRect(QRect(0, 0, 512, 512));
        std::vector<state_type> vs = sd->GetStates();
        std::vector<state_type>::iterator it;
        for(it=vs.begin(); it!=vs.end(); ++it) {
            //std::fprintf(stderr, "%d\n", (int)intFromVectorBool(*it));
            StateDiagramSceneState* s = new StateDiagramSceneState(*it);
            s->setPos(intFromVectorBool(*it)*60, 0);
            addItem(s);
            stateMap[*it] = s;
        }
        for(it=vs.begin(); it!=vs.end(); ++it) {
            StateDiagram::edges_type e = sd->GetEdges(*it);
            StateDiagram::edges_type::iterator eit;
            for(eit=e.begin(); eit!=e.end(); ++eit) {
                input_type input = eit->first;
                state_type nxtState = eit->second.first;
                output_type output = eit->second.second;
                if(nxtState!=STATE_INVALID) {
                    StateDiagramSceneArrow *arrow;
                    if(arrowMap.find(std::make_pair(*it, nxtState))
                            == arrowMap.end()) {
                        arrow = new StateDiagramSceneArrow();
                        arrow->setFrom(stateMap[*it]);
                        arrow->setTo(stateMap[nxtState]);
                        arrowMap[std::make_pair(*it, nxtState)] = arrow;
                        addItem(arrow);
                    }
                    else {
                        arrow = arrowMap[std::make_pair(*it, nxtState)];
                    }
                    arrow->addInputOutput(input, output);
                }
                else {
                }
            }
            /*for(size_t i=0, maxi=sd->GetInputNum();i<maxi;++i) { // each input
                input_type input = vectorBoolFromInt(i, sd->GetInputNum());
                state_type nxtState = sd->GetNextState(*it, input);
                if(nxtState!=STATE_INVALID) {
                    fprintf(stderr, "arrow!\n");
                    StateDiagramSceneArrow *arrow;
                    if(arrowMap.find(std::make_pair(*it, nxtState))
                            == arrowMap.end()) {
                        arrow = new StateDiagramSceneArrow();
                        arrow->setFrom(stateMap[*it]);
                        arrow->setTo(stateMap[nxtState]);
                        arrowMap[std::make_pair(*it, nxtState)] = arrow;
                        addItem(arrow);
                    }
                    else {
                        arrow = arrowMap[std::make_pair(*it, nxtState)];
                    }
                    arrow->addInput(input);
                }
                else {
                    fprintf(stderr, "BONG!\n");
                }
            }*/
        }
    }
    else {
        addText("No state diagram.");
    }
}

} // namespace ak47

