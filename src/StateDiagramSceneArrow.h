#ifndef AK47__STATEDIAGRAMSCENEARROW_H
#define AK47__STATEDIAGRAMSCENEARROW_H

#include <QGraphicsObject>
#include <QPainterPath>
#include <QString>

#include "StateDiagramSceneState.h"
#include "Types.h"

namespace ak47
{
  
  class StateDiagramSceneArrow : public QGraphicsObject
  {
    Q_OBJECT
  public:
    explicit StateDiagramSceneArrow(QGraphicsObject *parent = 0);
    
    void setFrom(StateDiagramSceneState*);
    void setTo(StateDiagramSceneState*);
    void addInputOutput(const input_type& input, const output_type& output);
    
    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    
    private slots:
    void fromMoved();
    void toMoved();
    
  private:
    void updatePath();
    
    StateDiagramSceneState *_from, *_to;
    QString _io;
    bool first;
    
    QPainterPath path;
    QPolygonF arrowHead;
    QPainterPath pathText;
    
  };
  
} // namespace ak47
#endif // AK47__STATEDIAGRAMSCENEARROW_H
