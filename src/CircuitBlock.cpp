#include "CircuitBlock.h"

#include <algorithm>
#include <iostream>
#include <iterator>

namespace ak47
{

namespace
{

template <class S, class T>
struct DeleteSecond
{
  void operator() (const std::pair<S, T>& p)
  {
    delete p.second;
  }
};

struct CountStateBit
{
private:
  size_t _result;
public:
  CountStateBit(): _result(0) {}
  void operator() (const std::pair<id_type, const CircuitComponent*>& p)
  {
    _result += p.second->GetStateBitNum();
  }
  size_t result()
  {
    return _result;
  }
};

} // unnamed namespace

struct CircuitBlock::AccumulateNextState
{
private:
  const CircuitBlock* _block;
  const state_type _state;
  const input_type _input;
  state_type _result;
public:
  AccumulateNextState(const CircuitBlock* block, state_type state, input_type input)
      : _block(block), _state(state), _input(input), _result() {}
  
  void operator() (const std::pair<id_type, const CircuitComponent*>& p)
  {
    //state_type o = p.second->GetNextState(_state, _input);
    state_type o = _block->GetComponentNextState(p.first, _state, _input);
    copy(o.begin(), o.end(),
        std::insert_iterator<state_type>(_result, _result.end()));
  }

  state_type result()
  {
    return _result;
  }
};

state_type
CircuitBlock::GetComponentState(id_type id, state_type state) const
{
  state_type result;
  size_t pos = 0;
  std::map<id_type, const CircuitComponent*>::const_iterator it = _comps.begin();
  for(; it!=_comps.end(); ++it) {
    size_t sbn = it->second->GetStateBitNum();
    if(id!=it->second->GetId()) {
      pos += sbn;
    }
    else {
      std::copy(state.begin()+pos, state.begin()+pos+sbn,
                std::insert_iterator<state_type>(result, result.end()));
      return result;
    }
  }
  return result; // empty result = STATE_INVALID
}

input_type
CircuitBlock::GetComponentInput(id_type id, state_type state, input_type input) const
{
  input_type result(GetComponentById(id)->GetInputNum());
  for(size_t i=0, maxi=result.size(); i<maxi; ++i) {
    if(_compsWireFrom.find(side_type(id, i))==_compsWireFrom.end()) {
      std::cerr << "Inside GetComponentInput: COMP NOT FOUND.\n";
    }
    side_type from = _compsWireFrom.find(side_type(id, i))->second;
    if(from.first==ID_PARENT) {
      result[i] = input[from.second];
    }
    else {
      result[i] = GetComponentOutput(from.first, state, input)[from.second];
    }
  }
  return result;
}

output_type
CircuitBlock::GetComponentOutput(id_type id, state_type state, input_type input) const
{
  if(_comps.find(id)==_comps.end()) {
    std::cerr << "ERROR: component not find\n";
  }
  const CircuitComponent* c = _comps.find(id)->second;
  state_type s = GetComponentState(id, state);
  if(s!=STATE_INVALID) {
    return s;
  }
  input_type i = GetComponentInput(id, state, input);
  return c->GetOutput(s, i);
}

state_type
CircuitBlock::GetComponentNextState(id_type id, state_type state, input_type input) const
{
  if(_comps.find(id)==_comps.end()) {
    std::cerr << "ERROR: component not find\n";
  }
  const CircuitComponent* c = _comps.find(id)->second;
  state_type s = GetComponentState(id, state);
  input_type i = GetComponentInput(id, state, input);
  return c->GetNextState(s, i);
}

const CircuitComponent*
CircuitBlock::GetComponentById(id_type id) const
{
  if(_comps.find(id)==_comps.end()) {
    return NULL;
  }
  return _comps.find(id)->second;
}

CircuitBlock::CircuitBlock(
    size_type inputNum,
    size_type outputNum,
    const std::string& name,
    std::map<id_type, const CircuitComponent*> comps,
    std::map<side_type, side_type> compsWireFrom,
    id_type id
): CircuitComponent(id),
  _inputNum(inputNum),
  _outputNum(outputNum),
  _name(name),
  _comps(comps),
  _compsWireFrom(compsWireFrom)
{
}


CircuitBlock::~CircuitBlock()
{
  std::for_each(_comps.begin(), _comps.end(), DeleteSecond<id_type, const CircuitComponent*>());
}

size_t
CircuitBlock::GetInputNum() const
{
  return _inputNum;
}

size_t
CircuitBlock::GetOutputNum() const
{
  return _outputNum;
}

size_t
CircuitBlock::GetStateBitNum() const
{
  return for_each(_comps.begin(), _comps.end(), CountStateBit()).result();
}

std::string
CircuitBlock::GetName() const
{
  return _name;
}

output_type
CircuitBlock::GetOutput(state_type state, input_type input) const
{
  output_type result;
  for(size_type i=0; i<GetOutputNum(); ++i) {
    side_type from = _compsWireFrom.find(side_type(ID_PARENT, i))->second;
    if(from.first==ID_PARENT) {
      result.push_back(input[from.second]);
    }
    else {
      result.push_back(GetComponentOutput(from.first, state, input)[from.second]);
    }
  }
  return result;
}

state_type
CircuitBlock::GetNextState(state_type state, input_type input) const
{
  return for_each(_comps.begin(), _comps.end(), AccumulateNextState(this, state, input)).result();
}

} // namespace ak47

