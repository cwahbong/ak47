#ifndef AK47__CIRCUITDESC_H
#define AK47__CIRCUITDESC_H

#include "Types.h"

#include <string>
#include <vector>

namespace ak47
{

class CircuitDesc
{
private:
  const size_t _inputNum;
  const size_t _outputNum;
  const size_t _stateBitNum;
  const std::string _name;
  /**
   * Prevent copy.  This will not be implemented at cpp.
   */
  CircuitDesc(const CircuitDesc&);
  /**
   * Prevent assign.  This will not be implemented at cpp.
   */
  CircuitDesc& operator=(const CircuitDesc&);
protected:
  CircuitDesc(size_t, size_t, size_t, const std::string&);
public:
  size_t GetInputNum() const;
  size_t GetOutputNum() const;
  size_t GetStateBitNum() const;
  std::string GetName() const;
  virtual output_type GetOutput(state_type state, input_type input) const = 0;
  virtual state_type GetNextState(state_type state, input_type input) const = 0;
};

} // namespace ak47
#endif // AK47__CIRCUITDESC_H

