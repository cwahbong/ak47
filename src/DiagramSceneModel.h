#ifndef AK47__DIAGRAMSCENEMODEL_H
#define AK47__DIAGRAMSCENEMODEL_H

#include <QObject>
#include <QMap>
#include <QSet>
#include <QPair>
#include "CircuitCreator.h"
#include "StateDiagram.h"
#include <string>

namespace ak47{
  
  class DiagramSceneItem;
  class DiagramSceneLine;
  
  class DiagramSceneModel : public QObject{
    Q_OBJECT
  public:
    DiagramSceneModel(QObject* parent);
    virtual ~DiagramSceneModel();
    
    StateDiagram getStateDiagram();
    
    void addItem(DiagramSceneItem* obj);
    void addWire(DiagramSceneLine* obj);
    
  private slots:
    void onItemDestroyed(QObject* obj);
    void onWireDestroyed(QObject* obj);
    
  private:
    typedef CircuitBlockBuilder::side_type side_type;
    
    bool isInputPort(const std::string& type);
    bool isOutputPort(const std::string& type);
    bool isComponent(const std::string& type);
    id_type getId
    (const DiagramSceneItem* item, const QMap<QObject*, id_type>& comp);
    
    CircuitCreator* creator;
    
    QList<QObject*> componentSet;
    QList<QObject*> blockInputList;
    QList<QObject*> blockOutputList;
    
    QSet<QObject*> connectionSet;
    
  };
  
}

#endif // AK47__DIAGRAMSCENEMODEL_H
