#include "StateDiagramSceneArrow.h"

#include <QPainter>

#include <algorithm>

namespace ak47
{
  
  StateDiagramSceneArrow::StateDiagramSceneArrow(QGraphicsObject *parent) :
  QGraphicsObject(parent), _from(NULL), _to(NULL), first(true)
  {
    this->setZValue(-1.0);
  }
  
  void StateDiagramSceneArrow::setFrom(StateDiagramSceneState* from)
  {
    _from = from;
    
    connect((QObject*)_from, SIGNAL(xChanged()),
            (QObject*)this, SLOT(fromMoved()));
    connect((QObject*)_from, SIGNAL(yChanged()),
            (QObject*)this, SLOT(fromMoved()));
    connect((QObject*)_from, SIGNAL(zChanged()),
            (QObject*)this, SLOT(fromMoved()));
    
    fromMoved();
  }
  
  void StateDiagramSceneArrow::setTo(StateDiagramSceneState* to)
  {
    _to = to;
    connect((QObject*)_to, SIGNAL(xChanged()),
            (QObject*)this, SLOT(toMoved()));
    connect((QObject*)_to, SIGNAL(yChanged()),
            (QObject*)this, SLOT(toMoved()));
    connect((QObject*)_to, SIGNAL(zChanged()),
            (QObject*)this, SLOT(toMoved()));
    
    toMoved();
  }
  
  void StateDiagramSceneArrow::addInputOutput(const input_type& input,
                                              const output_type& output)
  {
    if(!first) {
      _io.append(", ");
    }
    _io.append(stringFromVectorBool(input).c_str());
    _io.append("/");
    _io.append(stringFromVectorBool(output).c_str());
    first = false;
    updatePath();
  }
  
  QRectF StateDiagramSceneArrow::boundingRect() const
  {
    QRectF rt = path.boundingRect();
    rt = rt.united(pathText.boundingRect());
    rt = rt.united(arrowHead.boundingRect());
    return rt;
  }
  
  void StateDiagramSceneArrow::paint
  (QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
  {
    //
    painter->setPen(QPen(QBrush(Qt::black), 1));
    painter->setBrush(QBrush(Qt::transparent));
    
    painter->drawPath(path);
    
    //
    painter->setBrush(QBrush(Qt::black));
    
    painter->drawPolygon(arrowHead);
    painter->drawPath(pathText);
    
  }
  
  void StateDiagramSceneArrow::fromMoved()
  {
    updatePath();
  }
  
  void StateDiagramSceneArrow::toMoved()
  {
    updatePath();
  }
  
  void StateDiagramSceneArrow::updatePath()
  {
    // notify the scene that the geometry is to be changed
    this->prepareGeometryChange();
    
    // reset path
    path = QPainterPath();
    arrowHead = QPolygonF();
    pathText = QPainterPath();
    
    if(_from == NULL || _to == NULL)
      return;
    
    if(_from != _to){
      QRectF rt;
      
      rt = _from->mapRectToScene(_from->boundingRect());
      QPointF fromPos = rt.center();
      
      rt = _to->mapRectToScene(_to->boundingRect());
      QPointF toPos = rt.center();
      
      // move Origin
      this->setPos(fromPos);
      
      path.moveTo(0, 0); // from
      
      QPointF pto = this->mapFromScene(toPos);
      path.quadTo(QPointF(-pto.y(), pto.x())/16+(pto/2), pto); // to
      //path.lineTo( this->mapFromScene(toPos) ); // to 
    }else{
      QPointF pos = _to->mapRectToScene(_to->boundingRect()).center();
      this->setPos(pos);
      
      path.moveTo(0,0);
      path.lineTo(0,-25);
      path.quadTo(QPointF(20,-45), QPointF(0,-50));
      path.quadTo(QPointF(-20,-45), QPointF(0,-25));
      path.lineTo(0,0);
      //path.addEllipse(QPointF(0,-25), 25, 50);
    }

    ////////
    
    // add arrow
    
    {
      qreal pa = path.percentAtLength(path.length() - 25);
      qreal pa1 = path.percentAtLength(path.length() - 25 - 10);
      if(pa <= 0.0 || pa1 <= 0.0)
        return;
      
      QPointF pt0 = path.pointAtPercent(pa);
      QPointF pt1 = path.pointAtPercent(pa1);
      QPointF d = pt1 - pt0;
      QPointF d0 = QPointF(-d.y(), d.x()) / 2;
      QPointF d1 = -d0;
      
      arrowHead << (pt0) << (pt1 + d0) << (pt1 + d1) << (pt0);
    }
    
    //////////
    
    // add text
    pathText.addText(path.pointAtPercent(0.33), QFont(), _io);
    
  }
  
} // namespace ak47
