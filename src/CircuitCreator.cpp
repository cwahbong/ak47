#include "CircuitCreator.h"

#include "DescriptedComponent.h"

namespace ak47
{

const CircuitComponent*
CircuitCreator::NewCircuitComponent(const std::string& type, id_type id) const
{
  // NOTE THAT THE TYPE NAME SHOULD NOT DUPLICATED!!!
  for(size_t i=0; i<_vdm.size(); ++i) {
    if(_vdm[i]->GetDesc(type)!=NULL) {
      return new DescriptedComponent(_vdm[i]->GetDesc(type), id);
    }
  }
  return NULL;
}

const CircuitBlock*
CircuitCreator::NewCircuitBlock(const CircuitBlockBuilder& cbb) const
{
  if(!cbb.Validate()) {
    return NULL;
  }
  return cbb.Build();
}

} // namespace ak47

