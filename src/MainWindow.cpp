#include <QMetaObject>
#include <QMessageBox>

#include <algorithm>
#include <functional>
#include <cstdio>

#include "MainWindow.h"
#include "StateDiagramWindow.h"
#include "Types.h"
#include "ui_AboutDialog.h"

namespace ak47{


  MainWindow::MainWindow()
  {
    // Initialize UI
    setupUi(this);
    
    // add group to 'Palette' menu actions
    paletteActionGroup = new QActionGroup(this);
    {
      QList<QAction*> actions = menuPalette->actions(); 
      std::for_each(
        actions.begin(),
        actions.end(),
        std::bind1st(
          std::mem_fun<QAction*, QActionGroup, QAction*>(&QActionGroup::addAction),
          paletteActionGroup
        )
      );
    }
     
    // Enable Auto Connection
    // http://developer.qt.nokia.com/doc/qt-4.8/designer-using-a-ui-file.html

    // This is unneeded.
    //QMetaObject::connectSlotsByName(this);
    
    // Create Model
    model = new DiagramSceneModel(this);
    
    // Create Scene
    scene = new DiagramScene(model, this);
    scene->setSceneRect(QRectF(0, 0, 2048, 2048));
    mainDiagramView->setScene(scene);
    
  }
  
  MainWindow::~MainWindow()
  {
  }
  
  void MainWindow::on_actionMove_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::MOVE);
    }
  }
  
  void MainWindow::on_actionWire_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDLINE);
    }
  }
  
  void MainWindow::on_actionInput_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "PORT:IN");
    }
  }
  
  void MainWindow::on_actionOutput_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "PORT:OUT");
    }
  }
  
  void MainWindow::on_actionAND_Gate_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "GATE:AND");
    }
  }
  
  void MainWindow::on_actionOR_Gate_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "GATE:OR");
    }
  }
  
  void MainWindow::on_actionNOT_Gate_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "GATE:NOT");
    }
  }
  
  void MainWindow::on_actionD_Flip_Flop_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "FLIPFLOP:D");
    }
  }
  
  void MainWindow::on_actionT_Flip_Flop_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "FLIPFLOP:T");
    }
  }
  
  void MainWindow::on_actionJK_Flip_Flop_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "FLIPFLOP:JK");
    }
  }
  
  void MainWindow::on_actionSR_Flip_Flop_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "FLIPFLOP:SR");
    }
  }
  
  void MainWindow::on_actionNAND_Gate_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "GATE:NAND");
    }
  }
  
  void MainWindow::on_actionNOR_Gate_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "GATE:NOR");
    }
  }
  
  void MainWindow::on_actionXOR_Gate_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "GATE:XOR");
    }
  }
  
  void MainWindow::on_actionTRUE_Gate_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "GATE:TRUE");
    }
  }
  
  void MainWindow::on_actionFALSE_Gate_toggled(bool checked)
  {
    if(checked){
      scene->setMode(DiagramScene::ADDITEM, "GATE:FALSE");
    }
  }

  void MainWindow::on_actionBuild_State_Diagram_triggered()
  {
    /*
    StateDiagram* sd = new StateDiagram();
    sd->AddState(vectorBoolFromInt(0, 2));
    sd->AddState(vectorBoolFromInt(1, 2));
    sd->AddState(vectorBoolFromInt(2, 2));
    sd->AddState(vectorBoolFromInt(3, 2));
    sd->SetNextState(vectorBoolFromInt(2, 2), vectorBoolFromInt(1, 2),
                     vectorBoolFromInt(3, 2), vectorBoolFromInt(0, 2));
    sd->SetNextState(vectorBoolFromInt(2, 2), vectorBoolFromInt(3, 2),
                     vectorBoolFromInt(3, 2), vectorBoolFromInt(0, 2));
    StateDiagramWindow *w = new StateDiagramWindow(this, sd);
    w->show();*/
    
    StateDiagram* sd = new StateDiagram(model->getStateDiagram());
    StateDiagramWindow *w = new StateDiagramWindow(this, sd);
    w->show();
  }
  
  void MainWindow::on_actionAbout_triggered()
  {
    //std::printf("about me.\n");
    
    //QString text;
    //text += "Digital Logic Reverse Engineering Tool\n";
    //text += "Version 1.0\n";
    
    //QMessageBox::about(this, "About", text);
    
    Ui_AboutDialog uiloader;
    QDialog dialog;
    
    uiloader.setupUi(&dialog);
    
    dialog.exec();
  }
  
}
