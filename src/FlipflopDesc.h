#ifndef AK47__FLIPFLOPDESC_H
#define AK47__FLIPFLOPDESC_H

#include "CircuitDesc.h"

namespace ak47
{

class FlipflopDesc: public CircuitDesc
{
protected:
  FlipflopDesc(size_t input_num, const std::string& name)
    :CircuitDesc(input_num, 1, 1, name) {}
public:
  virtual state_type GetNextState(state_type state, input_type input) const;
};

template <template<class State, class Arg> class UnaryStateFunctor>
class FlipflopDesc1: public FlipflopDesc
{
public:
  FlipflopDesc1(const std::string& name): FlipflopDesc(1, name){}
  
  virtual output_type
  GetOutput(state_type state, input_type input) const
  {
    return std::vector<bool>(1, UnaryStateFunctor<bool, bool>()(state[0], input[0]));
  }
};

template <template<class State, class Arg> class BinaryStateFunctor>
class FlipflopDesc2: public FlipflopDesc
{
public:
  FlipflopDesc2(const std::string& name): FlipflopDesc(2, name){}

  virtual output_type
  GetOutput(state_type state, input_type input) const
  {
    return std::vector<bool>(1, BinaryStateFunctor<bool, bool>()(state[0], input[0], input[1]));
  }
};

} // namespace ak47
#endif // AK47__FLIPFLOPDESC_H

