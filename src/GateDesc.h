#ifndef AK47__GATEDESC_H
#define AK47__GATEDESC_H

#include "CircuitDesc.h"

#include <functional>

namespace ak47
{

/**
 */
class GateDesc: public CircuitDesc
{
protected:
  GateDesc(size_t input_num, const std::string& name)
    :CircuitDesc(input_num, 1, 0, name) {}
public:
  virtual state_type GetNextState(state_type state, input_type input) const;
};


/**
 */
template <bool output>
class GateDesc0: public GateDesc
{
public:
  GateDesc0(const std::string& name): GateDesc(0, name){}
  
  virtual output_type
  GetOutput(state_type /*state*/, input_type /*input*/) const
  {
    return std::vector<bool>(1, output);
  }
};

/**
 */
template <template<class Arg> class UnaryLogicalFunctor>
class GateDesc1: public GateDesc
{
public:
  GateDesc1(const std::string& name): GateDesc(1, name) {}

  virtual output_type
  GetOutput(state_type /*state*/, input_type input) const
  {
    return std::vector<bool>(1, UnaryLogicalFunctor<bool>()(input[0]));
  }
};

/**
 */
template <template<class Arg> class BinaryLogicalFunctor>
class GateDesc2: public GateDesc
{
public:
  GateDesc2(const std::string& name): GateDesc(2, name) {}
  virtual output_type
  GetOutput(state_type /*state*/, input_type input) const
  {
    return std::vector<bool>(1, BinaryLogicalFunctor<bool>()(input[0], input[1]));
  }
};

} // namespace ak47
#endif // AK47__GATEDESC_H

