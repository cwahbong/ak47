#include "DiagramSceneState.h"
#include "DiagramSceneItem.h"
#include "DiagramSceneLine.h"
#include "DiagramSceneModel.h"

#include <QGraphicsSceneMouseEvent>
#include <QList>

#include <cstdio>
#include <cmath>

namespace ak47{
  
  DiagramSceneState::DiagramSceneState
  (QGraphicsScene* parent, DiagramSceneModel* model)
  :QObject(parent), parentScene(parent), parentModel(model)
  {
  }
  
  DiagramSceneState::~DiagramSceneState()
  {
  }
  
  ////////////////////////////////////
  
  DiagramSceneMoving::DiagramSceneMoving
  (QGraphicsScene* parent, DiagramSceneModel* model)
  :DiagramSceneState(parent, model)
  {
  }
  
  DiagramSceneMoving::~DiagramSceneMoving()
  {
    foreach(QGraphicsItem *item, parentScene->selectedItems()){
      item->setSelected(false);
    }
  }
  
  bool DiagramSceneMoving::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
  {
    return false;
  }
  
  bool DiagramSceneMoving::mousePressEvent(QGraphicsSceneMouseEvent* event)
  {
    return false;
  }
  
  bool DiagramSceneMoving::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
  {
    return false;
  }
  
  ////////////////////////////////////
  
  DiagramSceneAddLine::DiagramSceneAddLine
  (QGraphicsScene* parent, DiagramSceneModel* model)
  :DiagramSceneState(parent, model)
  {
    DiagramSceneItem::_setDefaultMouseEvent(false);
    DiagramSceneLine::_setDefaultMouseEvent(false);
    
    foreach(QGraphicsItem *item, parentScene->items()){
      connect((QGraphicsObject*)item, SIGNAL(inputPressed(QGraphicsObject*,int)),
              (QObject*)this, SLOT(startOnInputConn(QGraphicsObject*,int)));
      connect((QGraphicsObject*)item, SIGNAL(inputReleased(QGraphicsObject*,int)),
              (QObject*)this, SLOT(endOnInputConn(QGraphicsObject*,int)));
      
      connect((QGraphicsObject*)item, SIGNAL(outputPressed(QGraphicsObject*,int)),
              (QObject*)this, SLOT(startOnOutputConn(QGraphicsObject*,int)));
      connect((QGraphicsObject*)item, SIGNAL(outputReleased(QGraphicsObject*,int)),
              (QObject*)this, SLOT(endOnOutputConn(QGraphicsObject*,int)));
    }
    
    inputStart = NULL;
    outputStart = NULL;
    
    tempLine = NULL;
  }
  
  DiagramSceneAddLine::~DiagramSceneAddLine()
  {
    DiagramSceneItem::_setDefaultMouseEvent(true);
    DiagramSceneLine::_setDefaultMouseEvent(true);
    
    foreach(QGraphicsItem *item, parentScene->items()){
    }
    
    cancelTracking();
  }
  
  bool DiagramSceneAddLine::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
  {
    if(tempLine){
      if(outputStart)
        tempLine->setInputPoint(event->scenePos());
      else
        tempLine->setOutputPoint(event->scenePos());
    }
    
    return true;
  }
  
  bool DiagramSceneAddLine::mousePressEvent(QGraphicsSceneMouseEvent* event)
  {
    QGraphicsItem* item = parentScene->itemAt(event->scenePos(), QTransform());
    if(item)
      parentScene->sendEvent(item, event);
    return true;
  }
  
  bool DiagramSceneAddLine::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
  {
    QGraphicsItem* item = parentScene->itemAt(event->scenePos(), QTransform());
    if(item)
      parentScene->sendEvent(item, event);
    else{
      cancelTracking();
    }
    return true;
  }
  
  void DiagramSceneAddLine::startOnInputConn(QGraphicsObject* sender, int index)
  {
    inputStart = sender;
    inputStartIndex = index;
    
    //std::printf("startOnInputConnector\n");
    
    DiagramSceneItem* item = (DiagramSceneItem*)sender;
    QRectF rt = item->inputConnRect(index);
    rt = item->mapRectToScene(rt);
    
    // TEST
    tempLine = new DiagramSceneLine();
    tempLine->setInputPoint(rt.center());
    tempLine->setOutputPoint(rt.center());
    tempLine->setZValue(-1.0);
    
    parentScene->addItem(tempLine);
  }
  
  void DiagramSceneAddLine::endOnInputConn(QGraphicsObject* sender, int index)
  {
    // check start point
    if(!outputStart){
      cancelTracking();
      return;
    }
    
    // handle
    addLine(sender, index, outputStart, outputStartIndex);
    
    // finish
    outputStart = NULL;
  }
  
  void DiagramSceneAddLine::startOnOutputConn
  (QGraphicsObject* sender, int index)
  {
    outputStart = sender;
    outputStartIndex = index;
    
    //std::printf("startOnOutputConnector\n");
    
    DiagramSceneItem* item = (DiagramSceneItem*)sender;
    QRectF rt = item->outputConnRect(index);
    rt = item->mapRectToScene(rt);
    
    // TEST
    tempLine = new DiagramSceneLine();
    tempLine->setInputPoint(rt.center());
    tempLine->setOutputPoint(rt.center());
    tempLine->setZValue(-1.0);
    
    parentScene->addItem(tempLine);
  }
  
  void DiagramSceneAddLine::endOnOutputConn(QGraphicsObject* sender, int index)
  {
    // check start point
    if(!inputStart){
      cancelTracking();
      return;
    }
    
    // handle
    addLine(inputStart, inputStartIndex, sender, index);
    
    // finish
    inputStart = NULL;
  }
  
  void DiagramSceneAddLine::cancelTracking()
  {
    inputStart = NULL;
    outputStart = NULL;
    if(tempLine){
      parentScene->removeItem(tempLine);
      delete tempLine;
      tempLine = NULL;
    }
  }
  
  void DiagramSceneAddLine::addLine
  (QGraphicsObject* input, int inIndex, QGraphicsObject* output, int outIndex)
  {
    Q_CHECK_PTR(tempLine);
    
    connect((QObject*)tempLine, SIGNAL(suicide(QGraphicsItem*)), 
            (QObject*)parentScene, SLOT(onItemSuicide(QGraphicsItem*)));
    
    if(!tempLine->setConnectors((DiagramSceneItem*)input, inIndex,
                                (DiagramSceneItem*)output, outIndex))
    {
      // fail
      parentScene->removeItem(tempLine);
      delete tempLine;
    }else{
      // todo: notify model
      parentModel->addWire(tempLine);
    }
    
    tempLine = NULL;
  }
  
  ////////////////////////////////////
  
  DiagramSceneAddItem::DiagramSceneAddItem
  (QString className, QGraphicsScene* parent, DiagramSceneModel* model)
  :DiagramSceneState(parent, model), itemClass(className)
  {
    DiagramSceneItem* item = new DiagramSceneItem(itemClass);
    itemObj = item;
    
    // Initialize Template Item
    itemObj->setOpacity(0.5);
    itemObj->setZValue(1000.0f);
    parentScene->addItem(itemObj);
  }
  
  DiagramSceneAddItem::~DiagramSceneAddItem()
  {
    parentScene->removeItem(itemObj);
    delete itemObj;
  }
  
  bool DiagramSceneAddItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
  {
    Q_CHECK_PTR(itemObj);
    itemObj->setPos(event->scenePos());
    
    return true;
  }
  
  bool DiagramSceneAddItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
  {
    return true;
  }
  
  bool DiagramSceneAddItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
  {
    if(event->button() != Qt::LeftButton){
      return true;
    }
    
    DiagramSceneItem* item = new DiagramSceneItem(itemClass);
    item->setPos(event->scenePos());
    
    parentScene->addItem(item);
    
    // todo: notify model
    parentModel->addItem(item);
    
    return true;
  }
  
}
