#ifndef AK47__STATEDIAGRAMSCENE_H
#define AK47__STATEDIAGRAMSCENE_H

#include <QGraphicsScene>

#include "StateDiagram.h"
#include "StateDiagramSceneArrow.h"

#include <map>
#include <utility>

namespace ak47
{

class StateDiagramScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit StateDiagramScene(QObject *parent = 0, const StateDiagram* sd = NULL);

private:
    std::map<std::pair<state_type, state_type>, StateDiagramSceneArrow*> arrowMap;
    std::map<state_type, StateDiagramSceneState*> stateMap;

signals:

public slots:

};

} // namespace ak47
#endif // AK47__STATEDIAGRAMSCENE_H
