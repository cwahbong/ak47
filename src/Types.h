#ifndef AK47__TYPES_H
#define AK47__TYPES_H

#include <cstdlib>
#include <functional>
#include <string>
#include <vector>

/**
 * \file Types.h
 * Defines types and constants here.
 */

namespace ak47
{

typedef int id_type;
const id_type ID_DEFAULT = -1;
const id_type ID_PARENT = -2;
const id_type ID_MIN = 0;
const id_type ID_MAX = 2147483647;

typedef std::vector<bool> input_type;
typedef std::vector<bool> output_type;

std::vector<bool> vectorBoolFromInt(size_t i, size_t size);

size_t intFromVectorBool(const std::vector<bool>& vb);
std::string stringFromVectorBool(const std::vector<bool>& vb);

/**
 * \brief state integral type.
 *
 * Represents a state id, negative state id should not be used since it
 * has special meanings.  See virables for more detail.
 */
typedef std::vector<bool> state_type;
const state_type STATE_INVALID = state_type();

typedef size_t size_type;

template <class T>
struct logical_nand: std::binary_function<T, T, bool>
{
  bool
  operator() (const T& x, const T& y) const
  {
    return ! (x&&y);
  }
};

template <class T>
struct logical_nor: std::binary_function<T, T, bool>
{
  bool
  operator() (const T& x, const T& y) const
  {
    return ! (x||y);
  }
};

template <class T>
struct logical_xor: std::binary_function<T, T, bool>
{
  bool
  operator() (const T& x, const T& y) const
  {
    return x^y;
  }
};

template <class S, class T>
struct state_logical_d
{
  bool
  operator() (const S&, const T& d) const
  {
    return d;
  }
};

template <class S, class T>
struct state_logical_t
{
  bool
  operator() (const S& q, const T& t) const
  {
    return q^t;
  }
};


template <class S, class T>
struct state_logical_jk
{
  bool
  operator() (const S& q, const T& j, const T& k) const
  {
    return (j&(!q)) | ((!k)&q);
  }
};

template <class S, class T>
struct state_logical_sr
{
  bool
  operator() (const S& q, const T& s, const T& r) const
  {
    return s | (q&(!r));
  }
};

} // namespace ak47
#endif // AK47__TYPES_H

