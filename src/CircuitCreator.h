#ifndef AK47__CIRCUITCREATOR_H
#define AK47__CIRCUITCREATOR_H

#include "CircuitBlock.h"
#include "CircuitBlockBuilder.h"
#include "CircuitComponent.h"
#include "DescManager.h"
#include "Types.h"

#include <string>
#include <vector>

namespace ak47
{

class CircuitBlockBuilder;
class CircuitCreator
{
private:
  std::vector<const DescManager*> _vdm;
public:
  CircuitCreator() {
    _vdm.push_back(GetGateDescManager());
    _vdm.push_back(GetFlipflopDescManager());
  }
  /**
   * Get a new circuit component by its type name.
   *
   * Current support component:
   * GATE:TRUE, GATE:FALSE, GATE:NOT, GATE:AND, GATE:OR,
   * GATE:NAND, GATE:NOR, GATE:XOR,
   * FLIPFLOP:D, FLIPFLOP:T, FLIPFLOP:JK, FLIPFLOP:SR
   */
  virtual const CircuitComponent* NewCircuitComponent(const std::string& type, id_type id=ID_DEFAULT) const;
  virtual const CircuitBlock* NewCircuitBlock(const CircuitBlockBuilder& cbb) const;
};

} // namespace ak47
#endif // AK47__CIRCUITCREATOR_H

