#ifndef AK47__DESCRIPTEDCOMPONENT_H
#define AK47__DESCRIPTEDCOMPONENT_H

#include "CircuitComponent.h"
#include "CircuitDesc.h"
#include "Types.h"

#include <string>
#include <vector>
#include <utility>

namespace ak47
{

class DescriptedComponent: public CircuitComponent
{
private:
  const CircuitDesc* const _desc;

public:
  DescriptedComponent(
      const CircuitDesc* const core,
      id_type id = ID_DEFAULT
  );
  virtual ~DescriptedComponent() {};

  virtual size_t GetInputNum() const;
  virtual size_t GetOutputNum() const;
  virtual size_t GetStateBitNum() const;
  virtual std::string GetName() const;

  virtual output_type GetOutput(state_type state, input_type input) const;
  virtual state_type GetNextState(state_type state, input_type input) const;

};

} // namespace ak47
#endif // AK47__DESCRIPTEDCOMPONENT_H

