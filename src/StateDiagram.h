#ifndef AK47__STATEDIAGRAM_H
#define AK47__STATEDIAGRAM_H

#include "Types.h"

#include <map>
#include <vector>

namespace ak47
{

/**
 * \brief     Represents a state diagram.
 */
class StateDiagram
{
public:
  typedef std::map<input_type, std::pair<state_type, output_type> > edges_type;
private:
  /**
   * _statesMap[state].first is the out edge(input)
   * _statesMap[state].second.first is the next state.
   * _stateMap[state].second.second is the output.
   */
  std::map<state_type, edges_type> _statesMap;
  std::vector<state_type> _statesList;
public:
  /**
   * \brief           Add a state into the state diagram.
   * If the id is already exists in the diagram, the diagram will be
   * unchanged and return false.
   * \param state     The id of the state you want to insert.
   * \return          true if it successfully add a state, false
   *                  otherwise.
   */
  bool AddState(state_type state);

  /**
   * \brief           Get the number of the states.
   * \return          The number of the states in this state diagram.
   */
  size_t GetStateNum() const;

  /**
   * \brief Get all of the states.
   */
  std::vector<state_type> GetStates() const;
  /**
   * \brief           Get the next state of the specific state and input.
   * \param state     The state.
   * \param input     The input.
   * \return          The next state.
   */
  state_type GetNextState(state_type state, input_type input) const;
  edges_type GetEdges(state_type state) const;

  /**
   * \brief           Get the output of the specific state and input.
   * \param state     The state.
   * \param input     The input.
   * \return          The output.
   */
  output_type GetOutput(state_type state, input_type input) const;

  /**
   * \brief           Set the next state of the specifie state and
   *                  outedge.
   * \param state     The state.
   * \param input     The input.
   * \param next      The next state.
   * \param output    The output.
   * \return          true if it finished successfully. If the id of
   *                  the state does not exists the diagram it will
   *                  return false.
   */
   bool SetNextState(state_type state, input_type input, state_type next, output_type output);
};

} // namespace ak47
#endif // AK47__STATEDIAGRAM_H

