#ifndef AK47__STATEDIAGRAMSCENESTATE_H
#define AK47__STATEDIAGRAMSCENESTATE_H

#include <QGraphicsObject>

#include "Types.h"

namespace ak47
{

class StateDiagramSceneState : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit StateDiagramSceneState(
        const state_type& state = STATE_INVALID, QGraphicsObject *parent = 0);

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

private:
    QString text;

};

} // namespace ak47
#endif // STATEDIAGRAMSCENESTATE_H
