#ifndef AK47__ENGINEERINGALGO_H
#define AK47__ENGINEERINGALGO_H

#include "CircuitBlock.h"
#include "StateDiagram.h"

/**
 * \file EngineeringAlgo.h
 * \brief The reverse/forward engineering algorithms are defined here.
 *
 * Here only implements reverse engineering now.
 */

namespace ak47
{

/**
 * \brief Generate the state diagram by given circuit diagram.
 * \param cd    The given circuit diagram.
 * \return      The state diagram.
 */
StateDiagram ReverseEngineering(const CircuitBlock& cd);
/*
 * \brief Generate the circuit diagram by given state diagram.
 * \param sd    The given state diagram.
 * \return      The state diagram.
 */
// CircuitBlock ForwardEngineering(const StateDiagram& sd);

} // namespace ak47
#endif // AK47__ENGINEERINGALGO_H

