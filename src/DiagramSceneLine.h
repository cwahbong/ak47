#ifndef AK47__DIAGRAMSCENELINE_H
#define AK47__DIAGRAMSCENELINE_H

#include <QPoint>
#include <QGraphicsObject>
#include <QPainterPath>

namespace ak47{
  
  class DiagramSceneItem;
  
  class DiagramSceneLine : public QGraphicsObject
  {
    Q_OBJECT
  public:
    DiagramSceneLine();
    virtual ~DiagramSceneLine();
    
    bool setConnectors(DiagramSceneItem* _inputItem, int _inputIndex,
                       DiagramSceneItem* _outputItem, int _outputIndex);
    
    void setInputPoint(QPointF pt);
    void setOutputPoint(QPointF pt);
    
    virtual QRectF boundingRect()const;
    virtual QPainterPath shape()const;
    virtual void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
    
    int outputIndex()const;
    int inputIndex()const;
    const DiagramSceneItem* inputItemPtr()const;
    const DiagramSceneItem* outputItemPtr()const;
    
    static bool _defaultMouseEvent();
    static void _setDefaultMouseEvent(bool enable);
    
  private slots:
    void inputMoved();
    void outputMoved();
    void inputRemoved();
    void outputRemoved();
    
  signals:
    void suicide(QGraphicsItem* item);
    
  private:
    void updatePath();
    virtual void mousePressEvent(QGraphicsSceneMouseEvent * event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
    
  private:
    DiagramSceneItem* inputItem;
    DiagramSceneItem* outputItem;
    int inputConnectorIndex;
    int outputConnectorIndex;
    
    QPointF inputPt; // scene space
    QPointF outputPt; // scene space
    
    QPainterPath path;
    
    static bool _bDefaultMouseEvent;
  };
  
}

#endif
