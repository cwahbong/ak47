#include "DiagramScene.h"

#include <QList>
#include <QPen>
#include <QBrush>
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QKeyEvent>
#include <QKeySequence>
#include <QGraphicsSceneMouseEvent>

#include <cstdio>

namespace ak47{
  
  DiagramScene::DiagramScene(DiagramSceneModel* _model, QObject* parent)
  :QGraphicsScene(parent), state(NULL), model(_model)
  {
    setMode(MOVE);
  }
  
  DiagramScene::~DiagramScene()
  {
    if(state)
      delete state;
  }
  
  void DiagramScene::setMode(Mode mode, QString itemClass)
  {
    if(state)
      delete state;
    
    switch(mode){
      case MOVE:
        state = new DiagramSceneMoving(this, model);
        break;
      case ADDLINE:
        state = new DiagramSceneAddLine(this, model);
        break;
      case ADDITEM:
        state = new DiagramSceneAddItem(itemClass, this, model);
        break;
    }
    
    Q_CHECK_PTR(state);
    
  }
  
  void DiagramScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event)
  {
  }
  
  void DiagramScene::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent)
  {
    Q_CHECK_PTR(state);
    if(!state->mousePressEvent(mouseEvent))
      QGraphicsScene::mousePressEvent(mouseEvent);
  }
  
  void DiagramScene::mouseReleaseEvent(QGraphicsSceneMouseEvent* mouseEvent)
  {
    Q_CHECK_PTR(state);
    if(!state->mouseReleaseEvent(mouseEvent))
      QGraphicsScene::mouseReleaseEvent(mouseEvent);
  }
  
  void DiagramScene::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent)
  {
    Q_CHECK_PTR(state);
    if(!state->mouseMoveEvent(mouseEvent))
      QGraphicsScene::mouseMoveEvent(mouseEvent);
  }
  
  void DiagramScene::keyReleaseEvent(QKeyEvent* keyEvent)
  {
    if(keyEvent->matches(QKeySequence::Delete) ||
       keyEvent->matches(QKeySequence::Back) )
    {
      foreach(QGraphicsItem *item, this->selectedItems()){
        this->removeItem(item);
        delete item;
      }
    }
    
    QGraphicsScene::keyReleaseEvent(keyEvent);
  }
  
  void DiagramScene::onItemSuicide(QGraphicsItem* item)
  {
    this->removeItem(item);
    delete item;
  }
  
}
