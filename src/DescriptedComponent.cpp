#include "DescriptedComponent.h"

namespace ak47
{

DescriptedComponent::DescriptedComponent(
  const CircuitDesc* const core,
  id_type id
): CircuitComponent(id), _desc(core)
{
}

size_t
DescriptedComponent::GetInputNum() const
{
  return _desc->GetInputNum();
}

size_t
DescriptedComponent::GetOutputNum() const
{
  return _desc->GetOutputNum();
}

size_t
DescriptedComponent::GetStateBitNum() const
{
  return _desc->GetStateBitNum();
}

std::string
DescriptedComponent::GetName() const
{
  return _desc->GetName();
}

output_type 
DescriptedComponent::GetOutput(state_type state, input_type input) const
{
  return _desc->GetOutput(state, input);
}

state_type
DescriptedComponent::GetNextState(state_type state, input_type input) const
{
  return _desc->GetNextState(state, input);
}

} // namespace ak47

