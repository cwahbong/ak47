// Qt
#include <QApplication>

// Ak47
#include "MainWindow.h"

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);
  ak47::MainWindow window;
  window.show();
  return app.exec();
}
