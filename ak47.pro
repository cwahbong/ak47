TEMPLATE	= app
CONFIG		+= qt warn_on debug
QT			= core gui svg
OBJECTS_DIR	= build
RCC_DIR		= build
UI_DIR		= build
MOC_DIR		= build
DESTDIR		= bin
DLLDESTDIR	= bin
QTDIR_build:REQUIRES="contains(QT_CONFIG, large-config)"

#
TARGET		= Ak47

#
HEADERS		= ../ak47/src/*.h
SOURCES		= ../ak47/src/*.cpp
RESOURCES	= ../ak47/rc/*.qrc
FORMS		= ../ak47/rc/*.ui

#
macx {
  ICON = ../ak47/rc/AppIcon.icns
}
win32 {
  #RC_FILE = myapp.rc
#IDI_ICON1 ICON DISCARDABLE "myappico.ico"
}

# install
# target.path = 
# sources.files = 
# sources.path = 
# INSTALLS += target sources
